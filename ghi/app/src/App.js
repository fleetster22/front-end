import React from "react";
import AttendeesList from "./components/AttendeesList";
import Nav from "./components/Nav";
import MainPage from "./components/MainPage";
import AttendForm from "./components/AttendForm";
import LocationForm from "./components/LocationForm";
import ConferenceForm from "./components/ConferenceForm";
import PresentationForm from "./components/PresentationForm";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="">
            <Route index element={<MainPage />} />
          </Route>
        </Routes>
        <Routes>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
        </Routes>
        <Routes>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
        </Routes>
        <Routes>
          <Route path="attendees">
            <Route
              path=""
              element={<AttendeesList attendees={props.attendees} />}
            />
          </Route>
        </Routes>
        <Routes>
          <Route path="attendees">
            <Route path="new" element={<AttendForm />} />
          </Route>
        </Routes>
        <Routes>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
